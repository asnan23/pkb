using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace PKBOnline.Controllers
{
    public abstract class PKBOnlineControllerBase: AbpController
    {
        protected PKBOnlineControllerBase()
        {
            LocalizationSourceName = PKBOnlineConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
