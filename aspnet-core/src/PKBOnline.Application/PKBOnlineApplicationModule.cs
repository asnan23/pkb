﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using PKBOnline.Authorization;

namespace PKBOnline
{
    [DependsOn(
        typeof(PKBOnlineCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class PKBOnlineApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<PKBOnlineAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(PKBOnlineApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
