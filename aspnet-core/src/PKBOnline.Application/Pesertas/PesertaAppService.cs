﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using PKBOnline.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace PKBOnline.Pesertas
{
    [AbpAuthorize(PermissionNames.Pages_Pesertas)]
    public class PesertaAppService : CrudAppService<Peserta, PesertaDto>
    {
        public PesertaAppService(IRepository<Peserta, int> repository) : base(repository)
        {
        }
    }
}
