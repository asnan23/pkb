﻿using Abp.Application.Services.Dto;

namespace PKBOnline.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

