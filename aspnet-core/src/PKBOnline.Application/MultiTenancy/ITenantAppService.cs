﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using PKBOnline.MultiTenancy.Dto;

namespace PKBOnline.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

