using System.ComponentModel.DataAnnotations;

namespace PKBOnline.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}