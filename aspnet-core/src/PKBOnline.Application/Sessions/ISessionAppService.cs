﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PKBOnline.Sessions.Dto;

namespace PKBOnline.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
