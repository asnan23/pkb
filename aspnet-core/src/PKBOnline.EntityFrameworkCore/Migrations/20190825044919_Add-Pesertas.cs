﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PKBOnline.Migrations
{
    public partial class AddPesertas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pesertas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    f_name = table.Column<string>(nullable: true),
                    l_name = table.Column<string>(nullable: true),
                    nik = table.Column<int>(nullable: false),
                    cell_phone = table.Column<int>(nullable: false),
                    gender = table.Column<int>(nullable: false),
                    age = table.Column<int>(nullable: false),
                    religion = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    domicile_address = table.Column<string>(nullable: true),
                    dob = table.Column<DateTime>(nullable: false),
                    pob = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pesertas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pesertas");
        }
    }
}
