using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace PKBOnline.EntityFrameworkCore
{
    public static class PKBOnlineDbContextConfigurer
    {

        public static void Configure(DbContextOptionsBuilder<PKBOnlineDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PKBOnlineDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection);
        }
    }
}
