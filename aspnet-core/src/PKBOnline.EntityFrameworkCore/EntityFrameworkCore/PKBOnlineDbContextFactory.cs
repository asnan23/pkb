﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using PKBOnline.Configuration;
using PKBOnline.Web;

namespace PKBOnline.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class PKBOnlineDbContextFactory : IDesignTimeDbContextFactory<PKBOnlineDbContext>
    {
        public PKBOnlineDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PKBOnlineDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            PKBOnlineDbContextConfigurer.Configure(builder, configuration.GetConnectionString(PKBOnlineConsts.ConnectionStringName));

            return new PKBOnlineDbContext(builder.Options);
        }
    }
}
