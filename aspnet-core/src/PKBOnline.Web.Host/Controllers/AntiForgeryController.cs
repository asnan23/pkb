using Microsoft.AspNetCore.Antiforgery;
using PKBOnline.Controllers;

namespace PKBOnline.Web.Host.Controllers
{
    public class AntiForgeryController : PKBOnlineControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
