﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace PKBOnline.Localization
{
    public static class PKBOnlineLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(PKBOnlineConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(PKBOnlineLocalizationConfigurer).GetAssembly(),
                        "PKBOnline.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
