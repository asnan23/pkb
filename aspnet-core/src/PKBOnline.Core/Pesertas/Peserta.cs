﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PKBOnline.Pesertas
{
    public class Peserta :Entity<int>
    {
        public String f_name { get; set; }
        public String l_name { get; set; }
        public int nik { get; set; }
        public int cell_phone { get; set; }
        public int gender { get; set; }
        public int age { get; set; }
        public string religion { get; set; }
        public String address { get; set; }
        public String domicile_address { get; set; }
        public DateTime dob { get; set; }
        public String pob { get; set; }
    }
}
