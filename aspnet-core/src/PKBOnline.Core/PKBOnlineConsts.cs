﻿namespace PKBOnline
{
    public class PKBOnlineConsts
    {
        public const string LocalizationSourceName = "PKBOnline";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
