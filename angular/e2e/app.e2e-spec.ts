import { PKBOnlineTemplatePage } from './app.po';

describe('PKBOnline App', function() {
  let page: PKBOnlineTemplatePage;

  beforeEach(() => {
    page = new PKBOnlineTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
