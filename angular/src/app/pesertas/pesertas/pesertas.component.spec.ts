import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesertasComponent } from './pesertas.component';

describe('PesertasComponent', () => {
  let component: PesertasComponent;
  let fixture: ComponentFixture<PesertasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesertasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesertasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
